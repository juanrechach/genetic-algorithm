import { PhysicalMachine } from "./PhysicalMacine";
import { VirtualMachine } from "./VirtualMachine";

export interface Definition {
  readonly PM: [PhysicalMachine];
  readonly VM: [VirtualMachine];
}
