import { Assignment } from "./Assignment";

export interface Chromosome {
  assignments: Assignment[];
  fitness: number;
}