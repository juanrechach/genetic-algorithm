import { Chromosome } from "./Chromosome";

export interface Population {
  chromosomes: Chromosome[];
}
