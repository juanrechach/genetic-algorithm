export interface VirtualMachine {
  readonly id: number;
  readonly mainmemory: number;
  readonly cpuusage: number;
}
