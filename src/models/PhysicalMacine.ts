export interface PhysicalMachine {
  readonly id: number;
  readonly powermin: number;
  readonly powermax: number;
  readonly cores: number;
  readonly vmwareoverhead: number;
  readonly mainmemory: number;
}
