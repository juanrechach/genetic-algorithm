import { Definition } from './models/Definition';
import { PhysicalMachine } from './models/PhysicalMacine';
import { Chromosome } from './models/Chromosome';
import { Assignment } from './models/Assignment';
import { Population } from './models/Population';

const numMaxGeneration = 5000;
function runGeneticAlgorithm(definition: Definition, populationSize: number,
  amountSurvivingInPercentage: number, members: number): void {
  let numGeneration = 0;
  let timesBestChromosomesIsInfinite = 0;
  let population: Population = generateInitialRandomPopulation(definition, populationSize);

  population = evaluateFitnessForPopulation(population, definition)

  let lastNBestChromosomes: Chromosome[] = [];

  let bestChromosomes: Chromosome[] = select(population, amountSurvivingInPercentage);
  console.log(`Generation ${numGeneration}. Best Fitness:  ${bestChromosomes[0].fitness}`)
  const increaseTimesFindInfinite = () => {
    if (bestChromosomes[0].fitness === Number.MAX_VALUE) { timesBestChromosomesIsInfinite += 1; }
  }
  increaseTimesFindInfinite();
  
  saveLastBestSolutions(bestChromosomes, lastNBestChromosomes,members );

  while (!stop(lastNBestChromosomes, timesBestChromosomesIsInfinite) && numGeneration < numMaxGeneration) { // FIXME: numGeneration is provisional just to stop
    numGeneration += 1;
    
    population = generateNewPopulationFromChromosomes(bestChromosomes, populationSize);
    
    population = mutate(population, definition);
    
    population = evaluateFitnessForPopulation(population, definition);
    bestChromosomes = select(population, amountSurvivingInPercentage);
    saveLastBestSolutions(bestChromosomes, lastNBestChromosomes,members );
    
    increaseTimesFindInfinite();
    
    console.log(`Generation ${numGeneration}. Best Fitness:  ${bestChromosomes[0].fitness}`)
  }

  console.log(`Solution found at generation ${numGeneration}. `);
 console.log(JSON.stringify(bestChromosomes[0].assignments));

}

function saveLastBestSolutions(bestChromosomes: Chromosome[], lastNBestChromosomes: Chromosome[], members:  number) {
  const bestChromosome = bestChromosomes[0];
  lastNBestChromosomes.unshift(bestChromosome);
  if (lastNBestChromosomes.length > members) {
    lastNBestChromosomes.pop();
  }
}

function generateInitialRandomPopulation(definition: Definition, populationSize: number,
): Population {
  const chromosomes: Chromosome[] = [];

  for (let i = 0; i < populationSize; i += 1) {
    const populationTmp = generateIndividualChromosome(definition);
    chromosomes.push(populationTmp);
  }

  return {
    chromosomes
  };
}

function generateIndividualChromosome(definition: Definition): Chromosome {
  const population: Chromosome = { assignments: [], fitness: Number.MAX_VALUE };

  population.assignments = definition.VM.map((vmTmp) => {
    const physicalMachine: PhysicalMachine = getRandomPhysicalMachine(definition);
    return {
      pm: physicalMachine,
      vm: vmTmp,
    }
  });

  return population;
}

function getRandomPhysicalMachine(definition: Definition, idDifferentTo: number = -1): PhysicalMachine {
  let pmTmp: PhysicalMachine;
  let randomPosition;
  do {
    randomPosition = Math.floor(Math.random() * definition.PM.length)
    pmTmp = definition.PM[randomPosition];
  } while (pmTmp.id === idDifferentTo)

  return pmTmp;
}

function evaluateFitnessForPopulation(population: Population, definition: Definition,
): Population {
  population.chromosomes = population.chromosomes.map((chromosomeTmp) => {
    return calculateFitness(chromosomeTmp, definition)
  });
  return population;
}

function calculateFitness(population: Chromosome, definition: Definition): Chromosome {
  const fitness: number = definition.PM.reduce((total, pmTmp) => {
    const vmsAssignedToPm = population.assignments.filter((tmp => {
      return tmp.pm.id === pmTmp.id;
    }));

    if (total !== Number.MAX_VALUE) {
      return total + calculatePowerConsumption(vmsAssignedToPm, pmTmp);
    } else {
      return Number.MAX_VALUE;
    }
  }, 0);

  population.fitness = fitness;

  return population;
}

function calculatePowerConsumption(assignments: Assignment[], pm: PhysicalMachine): number {
  const cpuUsage = assignments.reduce((total, tmp) => {
    return total + tmp.vm.cpuusage;
  }, pm.vmwareoverhead);
  const mainMemory = assignments.reduce((total, tmp) => {
    return total + tmp.vm.mainmemory;
  }, 0);

  const power = pm.powermin + ((cpuUsage / pm.cores) * (pm.powermax - pm.powermin));

  return (mainMemory >= pm.mainmemory || cpuUsage >= pm.cores)?  Number.MAX_VALUE : power;
}

function stop(lastNBestChromosomes: Chromosome[], timesBestChromosomesIsInfinite: number): boolean {
  return lastNBestChromosomes.length >= 5 && lastNBestChromosomes.every((tmp) => {
    return tmp.fitness === lastNBestChromosomes[0].fitness;
  }) && (timesBestChromosomesIsInfinite > 100 || 
    lastNBestChromosomes[lastNBestChromosomes.length - 1].fitness !== Number.MAX_VALUE);
}

function sortChromosomes(population: Population): Chromosome[] {
  return population.chromosomes.sort((a: Chromosome, b: Chromosome) => {
    return a.fitness > b.fitness ? 1 : -1
  });
}

function select(population: Population, amountSurvivingInPercentage: number): Chromosome[] {
  const sortedChromosomes: Chromosome[] = sortChromosomes(population);
  
  let amountToSelect = sortedChromosomes.length * amountSurvivingInPercentage;
  if (amountToSelect < 2) { amountToSelect = 2 }

  return sortedChromosomes.slice(0, amountToSelect);
}

function generateNewPopulationFromChromosomes(bestChromosomes: Chromosome[], populationSize: number,
): Population {
  const nextChromosomes: Chromosome[] = bestChromosomes.slice(0, 2);

  for (let i = nextChromosomes.length; i < populationSize; i += 1) {
    const [parent1, parent2] = getParents(bestChromosomes);
    const chromosomeTmp: Chromosome = createNewChromosomeFromParents(parent1, parent2);
    nextChromosomes.push(chromosomeTmp);
  }

  return {
    chromosomes: nextChromosomes,
  }
}

function mutate(population: Population, definition: Definition): Population {
  const nextPopulation = population;
  const startI = Math.floor(population.chromosomes.length / 2);
  for (let i = startI; i < population.chromosomes.length; i += 1) {
    const chromosomeTmp = population.chromosomes[i];
    const assignmentPosition = Math.floor(Math.random() * chromosomeTmp.assignments.length);
    const physicalMachine: PhysicalMachine = getRandomPhysicalMachine(definition,
      chromosomeTmp.assignments[assignmentPosition].pm.id);

    chromosomeTmp.assignments[assignmentPosition].pm = physicalMachine;
  }

  return nextPopulation;
}

function getParents(chromosomes: Chromosome[]): [Chromosome, Chromosome] {
  const parent1Index = Math.floor(Math.random() * chromosomes.length);
  let parent2Index;
  do {
    parent2Index = Math.floor(Math.random() * chromosomes.length);
  } while (parent1Index === parent2Index);

  return [
    chromosomes[parent1Index],
    chromosomes[parent2Index],
  ]
}

function createNewChromosomeFromParents(parent1: Chromosome, parent2: Chromosome): Chromosome {
  const cutPos = Math.floor(Math.random() * parent1.assignments.length);

  const assignments = parent1.assignments.slice(0, cutPos).concat(parent2.assignments.slice(cutPos));
  const nextAssignments = cloneAssignments(assignments);

  return {
    assignments: nextAssignments,
    fitness: Number.MAX_VALUE,
  };
}

function cloneAssignments(toClone: Assignment[]): Assignment[] {
  return toClone.map((assignmentTmp) => {
    return Object.assign({}, assignmentTmp);
  });
}



export default runGeneticAlgorithm;