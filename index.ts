import { Definition } from "./src/models/Definition";
import runGeneticAlgorithm from "./src/geneticAlgorithm";

const readDefinitions = async (definitionsFileName: string): Promise<Definition> => {
  console.log(`Reading definitions from file ${definitionsFileName}`);

  if (definitionsFileName[0] !== "/") {
    definitionsFileName = `./${definitionsFileName}`;
  }

  return import(definitionsFileName);
};

(async () => {
  let definitionsFileName: string = process.argv[2] || "definition.json";
  try {
    // ponemos un rango de 10-50 para el número de individuos de la populación
    const definition: Definition = await readDefinitions(definitionsFileName);
    let populationSize: number = calculatePopulationSize(definition); 
    let memberSize: number = calculateBestChromosomesSaved(definition);

    let amountSurvivingInPercentage: number = +process.argv[4] || 0.4;

    console.log(`Running Genetic Algorithm with populationSize [${populationSize}], 
      amountSurvivingInPercentage [${amountSurvivingInPercentage}],
      condition to stop: the last ${memberSize} generations must have the same best individual
      `);

    runGeneticAlgorithm(definition, populationSize, amountSurvivingInPercentage, memberSize);
  } catch {
    console.error(`Definition file specified [${definitionsFileName}] could not be found`);
  }
})();
function calculatePopulationSize(definition: Definition) {
  return Math.min(Math.floor((definition.VM.length / 6)) + 30, 100)
}
function calculateBestChromosomesSaved(definition: Definition) {
  let memberSize: number =Math.floor(definition.VM.length / 20);
  return memberSize > 20 ? 20:
    memberSize < 5 ? 5 : 
    memberSize; 
}
