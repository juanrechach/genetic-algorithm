# Genetic Algorithm

## Installation

* Install the lastest [Node.js](https://nodejs.org/en/) LTS version, it contains the npm (node package manager)

* Install dependencies
```
$ npm install
```

## How to run the algorithm
```
$ npm start
```

Or:
```
$ npm start definition.json 10 0.3
```


### Parameters
* Path to the definition folder: it can be absolute or relative. Default is definition.json
* Population size: Size of the population. Default is 10.
* amountSurvivingInPercentage: Amount of chromosomes that survive to the next generation (in percentage). Values are between 0 and 1. Default is 0.5 (50 %).